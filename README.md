# Local Solid Pod + Selenium

This docker image is intended for use case testing of Solid apps using Selenium.

### Starting the Solid Pod

By default, the Solid Pod starts on `https://localhost:8443`.
```bash
./entrypoint.sh start
``` 

