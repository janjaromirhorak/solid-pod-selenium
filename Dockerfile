FROM nodesolidserver/node-solid-server

USER root

RUN apk upgrade --update-cache --available
RUN apk add bash curl

# install newer node.js
ENV ALPINE_MIRROR "http://dl-cdn.alpinelinux.org/alpine"
RUN echo "${ALPINE_MIRROR}/edge/main" >> /etc/apk/repositories
RUN rm /usr/local/bin/node && apk add --no-cache nodejs-current  --repository="http://dl-cdn.alpinelinux.org/alpine/edge/community"

# install serve (for serving the app) and selenium packages
RUN yarn global add serve selenium-side-runner geckodriver

USER ${PROCESS_USER}

ENTRYPOINT ["/usr/bin/env"]
CMD ["bash"]